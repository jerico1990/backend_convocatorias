
using Microsoft.EntityFrameworkCore.Metadata;
using convocatorias.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using System.Threading.Tasks;
using Oracle.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;
using Microsoft.EntityFrameworkCore;
namespace convocatorias
{
    public partial class ApplicationDbContext : DbContext
    {
        
      

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        // public ApplicationDbContext()
        // {
        // }
        
        // public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        //     : base(options)
        // {
        // }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // if (!optionsBuilder.IsConfigured)
            // {
            //     optionsBuilder.UseSqlServer(Helper.ConnectionString());
            // }
            
//             if (!optionsBuilder.IsConfigured)
//             {
// //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                 // optionsBuilder
//                 //     .UseLoggerFactory(ConsoleLoggerFactory)
//                 //     .UseOracle("DATA SOURCE=10.50.73.11:1521/DEVELOPER1;USER ID=SISENA_CONVOCATORIA;PASSWORD=D3v3l0p3r$" );
//             }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Ignore<ConvocatoriaLista>();
            //modelBuilder.Entity<IdentityUser>().ToTable("ConvocatoriaLista", t => t.ExcludeFromMigrations());
        }

        public DbSet<Convocatoria> Convocatorias { get; set; }
        public DbSet<ConvocatoriaListaPublica> ConvocatoriasListasPublica { get; set; }
        public DbSet<ConvocatoriaListaInterna> ConvocatoriasListasInterna { get; set; }
        public DbSet<ConvocatoriaConsultar> ConvocatoriaConsultar { get; set; }
        public DbSet<PostulanteConsultar> PostulanteConsultar { get; set; }
        public DbSet<PostulanteLista> PostulanteListas { get; set; }

        public DbSet<ConvocatoriaPostulante> ConvocatoriaPostulantes { get; set; }
        public DbSet<Trazabilidad> Trazabilidad { get; set; }
        public DbSet<TrazabilidadLista> TrazabilidadLista { get; set; }

        public DbSet<Postulante> Postulantes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        

    }
}