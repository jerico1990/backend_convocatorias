using AutoMapper;
using Microsoft.AspNetCore.Identity;
using convocatorias.DTOs;
using convocatorias.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.Utilidades
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<ConvocatoriaListaPublica, ConvocatoriaListaPublicaDTO>();
            CreateMap<ConvocatoriaListaInterna, ConvocatoriaListaInternaDTO>();
            CreateMap<ConvocatoriaConsultar, ConvocatoriaConsultarDTO>();
            CreateMap<PostulanteConsultar, PostulanteConsultarDTO>();
            CreateMap<PostulanteLista, PostulanteListaDTO>();

            CreateMap<TrazabilidadLista, TrazabilidadListaDTO>();
        }

    }
}