using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.Entidades
{
    [Table("TC_USUARIO")]
    public class Usuario
    {
        [Key]
        [Column("COD_USUARIO")]
        public int codUsuario { get; set; }

        [Column("TXT_USUARIO")]
        public string txtUsuario { get; set; }

        [Column("TXT_CLAVE")]
        public string txtClave { get; set; }

        [Column("INT_ESTADO")]
        public int intEstado { get; set; }
    }
}