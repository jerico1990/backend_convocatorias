using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.Entidades
{
    [Table("TMM_CONVOCATORIA_POSTULANTE")]
    public class ConvocatoriaPostulante
    {
        [Key]
        [Column("COD_CONVOCATORIA_POSTULANTE")]
        public int codConvocatoriaPostulante { get; set; }

        [Column("INT_CONVOCATORIA")]
        public int intConvocatoria { get; set; }

        [Column("INT_POSTULANTE")]
        public int intPostulante { get; set; }

        [StringLength(2)]
        [Column("INT_ESTADO_TRAZABILIDAD")]
        public int intEstadoTrazabilidad { get; set; }

        [StringLength(1)]
        [Column("INT_ESTADO_REGISTRO")]
        public int intEstadoRegistro { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_REGISTRO")]
        public DateTime fecFechaRegistro { get; set; }
    }
}