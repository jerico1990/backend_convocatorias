using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.Entidades
{
    //[Table("Convocatoria", Schema = "Convocatoria")]
    [Table("TG_TRAZABILIDAD")]
    public class Trazabilidad
    {
        [Key]
        [Column("COD_TRAZABILIDAD")]
        public int codTrazabilidad { get; set; }

        [StringLength(250)]
        [Column("TXT_CATEGORIA")]
        public string ? txtCategoria { get; set; }

        [StringLength(1)]
        [Column("INT_CODIGO")]
        public int ? intCodigo { get; set; }


        [StringLength(250)]
        [Column("TXT_DESCRIPCION")]
        public string ? txtDescripcion { get; set; }

        [StringLength(1)]
        [Column("INT_ESTADO_REGISTRO")]
        public int ? intEstadoRegistro { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_REGISTRO")]
        public DateTime ? fecFechaRegistro { get; set; }
    }
}