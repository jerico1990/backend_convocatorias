using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.Entidades
{
    //[Table("Convocatoria", Schema = "Convocatoria")]
    [Table("TMM_CONVOCATORIA")]
    public class Convocatoria
    {
        [Key]
        [Column("COD_CONVOCATORIA")]
        public int codConvocatoria { get; set; }
       
        [StringLength(250)]
        [Column("TXT_DESCRIPCION")]
        public string ? txtDescripcion { get; set; }

        [StringLength(25)]
        [Column("TXT_AVISO_ARCHIVO")]
        public string ? txtAvisoArchivo { get; set; }

        [StringLength(25)]
        [Column("TXT_TDR_ARCHIVO")]
        public string ? txtTdrArchivo { get; set; }

        [StringLength(25)]
        [Column("TXT_FORMATOS_ARCHIVO")]
        public string ? txtFormatosArchivo { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_INICIO")]
        public DateTime ? fecFechaInicio { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_FIN")]
        public DateTime ? fecFechaFin { get; set; }

        [StringLength(2)]
        [Column("INT_ESTADO_TRAZABILIDAD")]
        public int ? intEstadoTrazabilidad { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_VIGENTE")]
        public DateTime ? fecFechaVigente { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_CONCLUIDA")]
        public DateTime ? fecFechaConcluida { get; set; }

        [StringLength(25)]
        [Column("TXT_NRO_OS")]
        public string ? txtNroOs { get; set; }

        [StringLength(25)]
        [Column("TXT_OS_ARCHIVO")]
        public string ? txtOsArchivo { get; set; }

        [StringLength(1)]
        [Column("INT_ESTADO_REGISTRO")]
        public int ? intEstadoRegistro { get; set; }

        [DataType(DataType.Date)]
        [Column("FEC_FECHA_REGISTRO")]
        public DateTime ? fecFechaRegistro { get; set; }
    }
}