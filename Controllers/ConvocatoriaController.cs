
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using convocatorias.DTOs;
using convocatorias.Entidades;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Oracle.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;

namespace convocatorias.Controllers
{
    [ApiController]
    [Route("api/convocatoria")]
    public class ConvocatoriaController : ControllerBase
    {
        private readonly ILogger<ConvocatoriaController> logger;
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public ConvocatoriaController(
            ILogger<ConvocatoriaController> logger,
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.logger = logger;
            this.context = context;
            this.mapper = mapper;
        }

        

        [HttpPost("registrar")]
        public async Task<ActionResult> postRegistrar([FromBody] ConvocatoriaRegistrarDTO convocatoriaRegistrarDTO)
        {
            var p_txt_descripcion       = new OracleParameter("p_txt_descripcion",OracleDbType.NVarchar2,250);
            var p_txt_aviso_archivo     = new OracleParameter("p_txt_aviso_archivo",OracleDbType.NVarchar2,25);
            var p_txt_tdr_archivo       = new OracleParameter("p_txt_tdr_archivo",OracleDbType.NVarchar2,25);
            var p_txt_formatos_archivo  = new OracleParameter("p_txt_formatos_archivo",OracleDbType.NVarchar2,25);
            var p_fec_fecha_inicio      = new OracleParameter("p_fec_fecha_inicio",OracleDbType.NVarchar2,10);
            var p_fec_fecha_fin         = new OracleParameter("p_fec_fecha_fin",OracleDbType.NVarchar2,10);
            var rpta                    = new OracleParameter("rpta",OracleDbType.NVarchar2,ParameterDirection.Output);

            p_txt_descripcion.Value = convocatoriaRegistrarDTO.txtDescripcion;
            p_txt_aviso_archivo.Value = convocatoriaRegistrarDTO.txtAvisoArchivo;
            p_txt_tdr_archivo.Value = convocatoriaRegistrarDTO.txtTdrArchivo;
            p_txt_formatos_archivo.Value = convocatoriaRegistrarDTO.txtFormatosArchivo;
            p_fec_fecha_inicio.Value = convocatoriaRegistrarDTO.fecFechaInicio;
            p_fec_fecha_fin.Value = convocatoriaRegistrarDTO.fecFechaFin;

            string sql = "BEGIN CONV_SP_C_CONVOCATORIA(:p_txt_descripcion,:p_txt_aviso_archivo,:p_txt_tdr_archivo,:p_txt_formatos_archivo,:p_fec_fecha_inicio,:p_fec_fecha_fin,:rpta); END;";
            await context.Database.ExecuteSqlRawAsync(sql, new object[] { 
                p_txt_descripcion,
                p_txt_aviso_archivo,
                p_txt_tdr_archivo,
                p_txt_formatos_archivo,
                p_fec_fecha_inicio,
                p_fec_fecha_fin,
                rpta
            });
            return Ok(rpta.Value);
        }


        [HttpPut("actualizar")]
        public async Task<ActionResult> postActualizar([FromBody] ConvocatoriaActualizarDTO convocatoriaActualizarDTO)
        {
            var p_int_cod_convocatoria      = new OracleParameter("p_int_cod_convocatoria",OracleDbType.Int32,11);
            var p_txt_descripcion           = new OracleParameter("p_txt_descripcion",OracleDbType.NVarchar2,250);
            var p_txt_aviso_archivo         = new OracleParameter("p_txt_aviso_archivo",OracleDbType.NVarchar2,25);
            var p_txt_tdr_archivo           = new OracleParameter("p_txt_tdr_archivo",OracleDbType.NVarchar2,25);
            var p_txt_formatos_archivo      = new OracleParameter("p_txt_formatos_archivo",OracleDbType.NVarchar2,25);
            var p_fec_fecha_inicio          = new OracleParameter("p_fec_fecha_inicio",OracleDbType.NVarchar2,10);
            var p_fec_fecha_fin             = new OracleParameter("p_fec_fecha_fin",OracleDbType.NVarchar2,10);
            var p_int_estado_trazabilidad   = new OracleParameter("p_int_estado_trazabilidad",OracleDbType.Int32,11);
            var rpta                        = new OracleParameter("rpta",OracleDbType.NVarchar2,ParameterDirection.Output);

            p_int_cod_convocatoria.Value    = convocatoriaActualizarDTO.codConvocatoria;
            p_txt_descripcion.Value         = convocatoriaActualizarDTO.txtDescripcion;
            p_txt_aviso_archivo.Value       = convocatoriaActualizarDTO.txtAvisoArchivo;
            p_txt_tdr_archivo.Value         = convocatoriaActualizarDTO.txtTdrArchivo;
            p_txt_formatos_archivo.Value    = convocatoriaActualizarDTO.txtFormatosArchivo;
            p_fec_fecha_inicio.Value        = convocatoriaActualizarDTO.fecFechaInicio;
            p_int_estado_trazabilidad.Value = convocatoriaActualizarDTO.intEstadoTrazabilidad;
            p_fec_fecha_fin.Value           = convocatoriaActualizarDTO.fecFechaFin;

            string sql = "BEGIN CONV_SP_U_CONVOCATORIA(:p_int_cod_convocatoria,:p_txt_descripcion,:p_txt_aviso_archivo,:p_txt_tdr_archivo,:p_txt_formatos_archivo,:p_fec_fecha_inicio,:p_fec_fecha_fin,:p_int_estado_trazabilidad,:rpta); END;";
            await context.Database.ExecuteSqlRawAsync(sql, new object[] { 
                p_int_cod_convocatoria,
                p_txt_descripcion,
                p_txt_aviso_archivo,
                p_txt_tdr_archivo,
                p_txt_formatos_archivo,
                p_fec_fecha_inicio,
                p_fec_fecha_fin,
                p_int_estado_trazabilidad,
                rpta
            });
            return Ok(rpta.Value);
        }


        [HttpPut("actualizar-adjudicada")]
        public async Task<ActionResult> postActualizarAdjudicada([FromBody] ConvocatoriaActualizarAdjudicarDTO convocatoriaActualizarAdjudicarDTO)
        {
            var p_int_cod_convocatoria      = new OracleParameter("p_int_cod_convocatoria",OracleDbType.Int32,11);
            var p_txt_nro_os                = new OracleParameter("p_txt_nro_os",OracleDbType.NVarchar2,250);
            var p_txt_os_archivo            = new OracleParameter("p_txt_os_archivo",OracleDbType.NVarchar2,25);
            var p_int_estado_trazabilidad   = new OracleParameter("p_int_estado_trazabilidad",OracleDbType.Int32);
            var rpta                        = new OracleParameter("rpta",OracleDbType.NVarchar2,ParameterDirection.Output);

            p_int_cod_convocatoria.Value    = convocatoriaActualizarAdjudicarDTO.codConvocatoria;
            p_txt_nro_os.Value              = convocatoriaActualizarAdjudicarDTO.txtNroOs;
            p_txt_os_archivo.Value          = convocatoriaActualizarAdjudicarDTO.txtOsArchivo;
            p_int_estado_trazabilidad.Value = convocatoriaActualizarAdjudicarDTO.intEstadoTrazabilidad;

            string sql = "BEGIN CONV_SP_U_ADJUDICAR_CONVOCATORIA(:p_int_cod_convocatoria,:p_txt_nro_os,:p_txt_os_archivo,:p_int_estado_trazabilidad,:rpta); END;";
            await context.Database.ExecuteSqlRawAsync(sql, new object[] { 
                p_int_cod_convocatoria,
                p_txt_nro_os,
                p_txt_os_archivo,
                p_int_estado_trazabilidad,
                rpta
            });
            return Ok(rpta.Value);
        }

        [HttpDelete("eliminar")]
        public async Task<ActionResult> postEliminar([FromBody] ConvocatoriaEliminarDTO convocatoriaEliminarDTO)
        {
            var p_int_cod_convocatoria      = new OracleParameter("p_int_cod_convocatoria",OracleDbType.Int32,11);
            var rpta                        = new OracleParameter("rpta",OracleDbType.NVarchar2,ParameterDirection.Output);

            p_int_cod_convocatoria.Value    = convocatoriaEliminarDTO.codConvocatoria;

            string sql = "BEGIN CONV_SP_D_CONVOCATORIA(:p_int_cod_convocatoria,:rpta); END;";
            await context.Database.ExecuteSqlRawAsync(sql, new object[] { 
                p_int_cod_convocatoria,
                rpta
            });
            return Ok(rpta.Value);
        }


        [HttpGet("consultar/{codConvocatoria:int}")]
        //public async Task<ActionResult<List<ConvocatoriaConsultarDTO>>> getConsulta([FromBody] ConvocatoriaConsultaDTO convocatoriaConsultaDTO)
        public async Task<ActionResult<List<ConvocatoriaConsultarDTO>>> getConsulta(int codConvocatoria)
        {
            // Create REF Cursor output parameter
            var p_int_cod_convocatoria      = new OracleParameter("p_int_cod_convocatoria",OracleDbType.Int32,11);
            var p_recordset = new OracleParameter("p_recordset",OracleDbType.RefCursor, ParameterDirection.Output);

            p_int_cod_convocatoria.Value    = codConvocatoria;

            string sql = "BEGIN CONV_SP_R_CONVOCATORIA(:p_int_cod_convocatoria,:p_recordset); END;";
            var convocatoriaConsulta =await context.ConvocatoriaConsultar.FromSqlRaw(sql, new object[] { p_int_cod_convocatoria,p_recordset }).ToListAsync();
            return mapper.Map<List<ConvocatoriaConsultarDTO>>(convocatoriaConsulta);
        }


        [HttpGet("lista-publica")]
        public async Task<ActionResult<List<ConvocatoriaListaPublicaDTO>>> getListaPublica()
        {
            // Create REF Cursor output parameter
            var p_recordset = new OracleParameter("p_recordset",OracleDbType.RefCursor, ParameterDirection.Output);
            //string sql = "SELECT ID,DESCRIPCION FROM CONVOCATORIA";
            string sql = "BEGIN CONV_SP_LST_PUBLICA_CONVOCATORIAS(:p_recordset); END;";
            var convocatoriasPublicas =await context.ConvocatoriasListasPublica.FromSqlRaw(sql, new object[] { p_recordset }).ToListAsync();
            return mapper.Map<List<ConvocatoriaListaPublicaDTO>>(convocatoriasPublicas);
        }
        
        [HttpGet("lista-interna")]
        public async Task<ActionResult<List<ConvocatoriaListaInternaDTO>>> getListaInterna()
        {
            // Create REF Cursor output parameter
            var p_recordset = new OracleParameter("p_recordset",OracleDbType.RefCursor, ParameterDirection.Output);
            //string sql = "SELECT ID,DESCRIPCION FROM CONVOCATORIA";
            string sql = "BEGIN CONV_SP_LST_INTERNA_CONVOCATORIAS(:p_recordset); END;";
            var convocatoriasInternas =await context.ConvocatoriasListasInterna.FromSqlRaw(sql, new object[] { p_recordset }).ToListAsync();
            return mapper.Map<List<ConvocatoriaListaInternaDTO>>(convocatoriasInternas);
        }
    }
}
