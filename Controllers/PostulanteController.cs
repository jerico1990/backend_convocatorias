
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using convocatorias.DTOs;
using convocatorias.Entidades;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Oracle.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;

namespace convocatorias.Controllers
{
    [ApiController]
    [Route("api/postulante")]
    public class PostulanteController : ControllerBase
    {
        private readonly ILogger<PostulanteController> logger;
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public PostulanteController(
            ILogger<PostulanteController> logger,
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.logger = logger;
            this.context = context;
            this.mapper = mapper;
        }


        [HttpPost("registrar")]
        public async Task<ActionResult> postRegistrar([FromBody] PostulanteRegistrarDTO postulanteRegistrarDTO)
        {
            var p_int_cod_convocatoria      = new OracleParameter("p_int_cod_convocatoria",OracleDbType.Int32,11);
            var p_txt_nro_documento         = new OracleParameter("p_txt_nro_documento",OracleDbType.NVarchar2,8);
            var p_txt_nombres               = new OracleParameter("p_txt_nombres",OracleDbType.NVarchar2,50);
            var p_txt_apellidos             = new OracleParameter("p_txt_apellidos",OracleDbType.NVarchar2,50);
            var p_txt_correo_electronico    = new OracleParameter("p_txt_correo_electronico",OracleDbType.NVarchar2,50);
            var p_txt_celular               = new OracleParameter("p_txt_celular",OracleDbType.NVarchar2,9);
            var p_txt_region_id             = new OracleParameter("p_txt_region_id",OracleDbType.NVarchar2,2);
            var p_txt_provincia_id          = new OracleParameter("p_txt_provincia_id",OracleDbType.NVarchar2,4);
            var p_txt_distrito_id           = new OracleParameter("p_txt_distrito_id",OracleDbType.NVarchar2,6);
            var p_txt_direccion             = new OracleParameter("p_txt_direccion",OracleDbType.NVarchar2,100);
            var p_txt_formatos_archivo      = new OracleParameter("p_txt_formatos_archivo",OracleDbType.NVarchar2,25);
            var p_txt_cv_comercial_archivo  = new OracleParameter("p_txt_cv_comercial_archivo",OracleDbType.NVarchar2,25);
            var p_txt_cv_documentado_archivo= new OracleParameter("p_txt_cv_documentado_archivo",OracleDbType.NVarchar2,25);
            var rpta                        = new OracleParameter("rpta",OracleDbType.NVarchar2,ParameterDirection.Output);

            p_int_cod_convocatoria.Value        = postulanteRegistrarDTO.codConvocatoria;
            p_txt_nro_documento.Value           = postulanteRegistrarDTO.txtNroDocumento;
            p_txt_nombres.Value                 = postulanteRegistrarDTO.txtNombres;
            p_txt_apellidos.Value               = postulanteRegistrarDTO.txtApellidos;
            p_txt_correo_electronico.Value      = postulanteRegistrarDTO.txtCorreoElectronico;
            p_txt_celular.Value                 = postulanteRegistrarDTO.txtCelular;
            p_txt_region_id.Value               = postulanteRegistrarDTO.txtRegionId;
            p_txt_provincia_id.Value            = postulanteRegistrarDTO.txtProvinciaId;
            p_txt_distrito_id.Value             = postulanteRegistrarDTO.txtDistritoId;
            p_txt_direccion.Value               = postulanteRegistrarDTO.txtDireccion;
            p_txt_formatos_archivo.Value        = postulanteRegistrarDTO.txtFormatosArchivo;
            p_txt_cv_comercial_archivo.Value    = postulanteRegistrarDTO.txtCvComercialArchivo;
            p_txt_cv_documentado_archivo.Value  = postulanteRegistrarDTO.txtCvDocumentadoArchivo;


            string sql = "BEGIN CONV_SP_C_POSTULANTE(:p_int_cod_convocatoria,:p_txt_nro_documento,:p_txt_nombres,:p_txt_apellidos,:p_txt_correo_electronico,:p_txt_celular,:p_txt_region_id,:p_txt_provincia_id,:p_txt_distrito_id,:p_txt_direccion,:p_txt_formatos_archivo,:p_txt_cv_comercial_archivo,:p_txt_cv_documentado_archivo,:rpta); END;";
            await context.Database.ExecuteSqlRawAsync(sql, new object[] { 
                p_int_cod_convocatoria,
                p_txt_nro_documento,
                p_txt_nombres,
                p_txt_apellidos,
                p_txt_correo_electronico,
                p_txt_celular,
                p_txt_region_id,
                p_txt_provincia_id,
                p_txt_distrito_id,
                p_txt_direccion,
                p_txt_formatos_archivo,
                p_txt_cv_comercial_archivo,
                p_txt_cv_documentado_archivo,
                rpta
            });
            return Ok(rpta.Value);
        }


        [HttpGet("consultar")]
        public async Task<ActionResult<List<PostulanteConsultarDTO>>> getConsulta([FromBody] PostulanteConsultaDTO postulanteConsultaDTO)
        {
            // Create REF Cursor output parameter
            var p_int_cod_postulante      = new OracleParameter("p_int_cod_postulante",OracleDbType.Int32,11);
            var p_recordset = new OracleParameter("p_recordset",OracleDbType.RefCursor, ParameterDirection.Output);

            p_int_cod_postulante.Value    = postulanteConsultaDTO.codPostulante;

            string sql = "BEGIN CONV_SP_R_POSTULANTE(:p_int_cod_postulante,:p_recordset); END;";
            var postulanteConsulta =await context.PostulanteConsultar.FromSqlRaw(sql, new object[] { p_int_cod_postulante,p_recordset }).ToListAsync();
            return mapper.Map<List<PostulanteConsultarDTO>>(postulanteConsulta);
        }

        [HttpGet("lista")]
        public async Task<ActionResult<List<PostulanteListaDTO>>> getLista([FromBody] ConvocatoriaConsultaDTO convocatoriaConsultaDTO)
        {
            // Create REF Cursor output parameter
            var p_int_cod_convocatoria  = new OracleParameter("p_int_cod_convocatoria",OracleDbType.Int32,11);
            var p_recordset             = new OracleParameter("p_recordset",OracleDbType.RefCursor, ParameterDirection.Output);

            p_int_cod_convocatoria.Value    = convocatoriaConsultaDTO.codConvocatoria;

            string sql = "BEGIN CONV_SP_LST_POSTULANTES(:p_int_cod_convocatoria,:p_recordset); END;";
            var postulanteLista =await context.PostulanteListas.FromSqlRaw(sql, new object[] { p_int_cod_convocatoria,p_recordset }).ToListAsync();
            return mapper.Map<List<PostulanteListaDTO>>(postulanteLista);
        }
    }
}
