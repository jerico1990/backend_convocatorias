
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using convocatorias.DTOs;
using convocatorias.Entidades;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Oracle.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;

namespace convocatorias.Controllers
{
    [ApiController]
    [Route("api/trazabilidad")]
    public class Trazabilidadcontroller : ControllerBase
    {
        private readonly ILogger<Trazabilidadcontroller> logger;
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public Trazabilidadcontroller(
            ILogger<Trazabilidadcontroller> logger,
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.logger = logger;
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet("lista")]
        public async Task<ActionResult<List<TrazabilidadListaDTO>>> getLista()
        {
            // Create REF Cursor output parameter
            var p_recordset             = new OracleParameter("p_recordset",OracleDbType.RefCursor, ParameterDirection.Output);


            string sql = "BEGIN CONV_SP_LST_TRAZABILIDAD(:p_recordset); END;";
            var trazabilidadLista =await context.TrazabilidadLista.FromSqlRaw(sql, new object[] { p_recordset }).ToListAsync();
            return mapper.Map<List<TrazabilidadListaDTO>>(trazabilidadLista);
        }
    }
}
