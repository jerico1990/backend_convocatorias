using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.DTOs
{
    public class ConvocatoriaActualizarAdjudicarDTO
    {
        [Key]
        [Column("COD_CONVOCATORIA")]
        public int ? codConvocatoria { get; set; } 

        [StringLength(25)]
        [Column("TXT_NRO_OS")]
        public string ? txtNroOs { get; set; }

        [StringLength(25)]
        [Column("TXT_OS_ARCHIVO")]
        public string ? txtOsArchivo { get; set; }

        [StringLength(2)]
        [Column("INT_ESTADO_TRAZABILIDAD")]
        public int ? intEstadoTrazabilidad { get; set; }

    }
}