using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.DTOs
{
    public class PostulanteConsultaDTO
    {
        [Key]
        [Column("COD_POSTULANTE")]
        public int ? codPostulante { get; set; } 
       
    }
}