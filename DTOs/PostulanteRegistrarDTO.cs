using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.DTOs
{
    [Table("TMM_POSTULANTE")]
    public class PostulanteRegistrarDTO
    {
        [Column("INT_CONVOCATORIA")]
        public int ? codConvocatoria { get; set; } 


        [StringLength(8)]
        [Column("TXT_NRO_DOCUMENTO")]
        public string txtNroDocumento { get; set; }

        [StringLength(50)]
        [Column("TXT_NOMBRES")]
        public string txtNombres { get; set; }

        [StringLength(50)]
        [Column("TXT_APELLIDOS")]
        public string txtApellidos { get; set; }

        [StringLength(50)]
        [Column("TXT_CORREO_ELECTRONICO")]
        public string txtCorreoElectronico { get; set; }

        [StringLength(9)]
        [Column("TXT_CELULAR")]
        public string txtCelular { get; set; }

        [StringLength(2)]
        [Column("TXT_REGION_ID")]
        public string txtRegionId { get; set; }

        [StringLength(4)]
        [Column("TXT_PROVINCIA_ID")]
        public string txtProvinciaId { get; set; }

        [StringLength(6)]
        [Column("TXT_DISTRITO_ID")]
        public string txtDistritoId { get; set; }

        [StringLength(100)]
        [Column("TXT_DIRECCION")]
        public string txtDireccion { get; set; }

        [StringLength(25)]
        [Column("TXT_FORMATOS_ARCHIVO")]
        public string txtFormatosArchivo { get; set; }

        [StringLength(25)]
        [Column("TXT_CV_COMERCIAL_ARCHIVO")]
        public string txtCvComercialArchivo { get; set; }

        [StringLength(25)]
        [Column("TXT_CV_DOCUMENTADO_ARCHIVO")]
        public string txtCvDocumentadoArchivo { get; set; }

    }
}