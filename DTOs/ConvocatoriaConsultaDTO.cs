using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.DTOs
{
    public class ConvocatoriaConsultaDTO
    {
        [Key]
        [Column("COD_CONVOCATORIA")]
        public int ? codConvocatoria { get; set; } 
       
    }
}