using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace convocatorias.DTOs
{
    public class ConvocatoriaRegistrarDTO
    {
        [Required]
        [StringLength(250)]
        [Column("TXT_DESCRIPCION")]
        public string ? txtDescripcion { get; set; }

        [Required]
        [StringLength(25)]
        [Column("TXT_AVISO_ARCHIVO")]
        public string ? txtAvisoArchivo { get; set; }

        [Required]
        [StringLength(25)]
        [Column("TXT_TDR_ARCHIVO")]
        public string ? txtTdrArchivo { get; set; }

        [Required]
        [StringLength(25)]
        [Column("TXT_FORMATOS_ARCHIVO")]
        public string ? txtFormatosArchivo { get; set; }

        [Required]
        [StringLength(10)]
        [Column("FEC_FECHA_INICIO")]
        public string ? fecFechaInicio { get; set; }

        [Required]
        [StringLength(10)]
        [Column("FEC_FECHA_FIN")]
        public string ? fecFechaFin { get; set; }

    }
}