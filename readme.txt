dotnet add package Oracle.EntityFrameworkCore --version 5.21.1
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Tools

dotnet ef migrations add InitialCreate
dotnet ef database update
dotnet add package AutoMapper --version 10.1.1