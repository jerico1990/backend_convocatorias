﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Oracle.EntityFrameworkCore.Metadata;
using convocatorias;

namespace convocatorias.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20210511020910_SecondCreate")]
    partial class SecondCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.5")
                .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("convocatorias.Entidades.Convocatoria", b =>
                {
                    b.Property<int>("codConvocatoria")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("COD_CONVOCATORIA")
                        .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("fecFechaConcluida")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_CONCLUIDA");

                    b.Property<DateTime?>("fecFechaFin")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_FIN");

                    b.Property<DateTime?>("fecFechaInicio")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_INICIO");

                    b.Property<DateTime?>("fecFechaRegistro")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_REGISTRO");

                    b.Property<DateTime?>("fecFechaVigente")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_VIGENTE");

                    b.Property<int?>("intEstadoRegistro")
                        .HasMaxLength(1)
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ESTADO_REGISTRO");

                    b.Property<int?>("intEstadoTrazabilidad")
                        .HasMaxLength(2)
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ESTADO_TRAZABILIDAD");

                    b.Property<string>("txtAvisoArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_AVISO_ARCHIVO");

                    b.Property<string>("txtDescripcion")
                        .HasMaxLength(250)
                        .HasColumnType("NVARCHAR2(250)")
                        .HasColumnName("TXT_DESCRIPCION");

                    b.Property<string>("txtFormatosArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_FORMATOS_ARCHIVO");

                    b.Property<string>("txtNroOs")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_NRO_OS");

                    b.Property<string>("txtOsArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_OS_ARCHIVO");

                    b.Property<string>("txtTdrArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_TDR_ARCHIVO");

                    b.HasKey("codConvocatoria");

                    b.ToTable("TMM_CONVOCATORIA");
                });

            modelBuilder.Entity("convocatorias.Entidades.ConvocatoriaPostulante", b =>
                {
                    b.Property<int>("codConvocatoriaPostulante")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("COD_CONVOCATORIA_POSTULANTE")
                        .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("fecFechaRegistro")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_REGISTRO");

                    b.Property<int>("intConvocatoria")
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_CONVOCATORIA");

                    b.Property<int>("intEstadoRegistro")
                        .HasMaxLength(1)
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ESTADO_REGISTRO");

                    b.Property<int>("intEstadoTrazabilidad")
                        .HasMaxLength(2)
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ESTADO_TRAZABILIDAD");

                    b.Property<int>("intPostulante")
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_POSTULANTE");

                    b.HasKey("codConvocatoriaPostulante");

                    b.ToTable("TMM_CONVOCATORIA_POSTULANTE");
                });

            modelBuilder.Entity("convocatorias.Entidades.Postulante", b =>
                {
                    b.Property<int>("codPostulante")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("COD_POSTULANTE")
                        .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("fecFechaRegistro")
                        .HasColumnType("TIMESTAMP(7)")
                        .HasColumnName("FEC_FECHA_REGISTRO");

                    b.Property<int>("intAnio")
                        .HasMaxLength(4)
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ANIO");

                    b.Property<int>("intEstadoRegistro")
                        .HasMaxLength(1)
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ESTADO_REGISTRO");

                    b.Property<string>("txtApellidos")
                        .HasMaxLength(50)
                        .HasColumnType("NVARCHAR2(50)")
                        .HasColumnName("TXT_APELLIDOS");

                    b.Property<string>("txtCelular")
                        .HasMaxLength(9)
                        .HasColumnType("NVARCHAR2(9)")
                        .HasColumnName("TXT_CELULAR");

                    b.Property<string>("txtCorreoElectronico")
                        .HasMaxLength(50)
                        .HasColumnType("NVARCHAR2(50)")
                        .HasColumnName("TXT_CORREO_ELECTRONICO");

                    b.Property<string>("txtCvComercialArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_CV_COMERCIAL_ARCHIVO");

                    b.Property<string>("txtCvDocumentadoArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_CV_DOCUMENTADO_ARCHIVO");

                    b.Property<string>("txtDireccion")
                        .HasMaxLength(100)
                        .HasColumnType("NVARCHAR2(100)")
                        .HasColumnName("TXT_DIRECCION");

                    b.Property<string>("txtDistritoId")
                        .HasMaxLength(6)
                        .HasColumnType("NVARCHAR2(6)")
                        .HasColumnName("TXT_DISTRITO_ID");

                    b.Property<string>("txtFormatosArchivo")
                        .HasMaxLength(25)
                        .HasColumnType("NVARCHAR2(25)")
                        .HasColumnName("TXT_FORMATOS_ARCHIVO");

                    b.Property<string>("txtNombres")
                        .HasMaxLength(50)
                        .HasColumnType("NVARCHAR2(50)")
                        .HasColumnName("TXT_NOMBRES");

                    b.Property<string>("txtNroDocumento")
                        .HasMaxLength(8)
                        .HasColumnType("NVARCHAR2(8)")
                        .HasColumnName("TXT_NRO_DOCUMENTO");

                    b.Property<string>("txtProvinciaId")
                        .HasMaxLength(4)
                        .HasColumnType("NVARCHAR2(4)")
                        .HasColumnName("TXT_PROVINCIA_ID");

                    b.Property<string>("txtRegionId")
                        .HasMaxLength(2)
                        .HasColumnType("NVARCHAR2(2)")
                        .HasColumnName("TXT_REGION_ID");

                    b.HasKey("codPostulante");

                    b.ToTable("TMM_POSTULANTE");
                });

            modelBuilder.Entity("convocatorias.Entidades.Usuario", b =>
                {
                    b.Property<int>("codUsuario")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("COD_USUARIO")
                        .HasAnnotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("intEstado")
                        .HasColumnType("NUMBER(10)")
                        .HasColumnName("INT_ESTADO");

                    b.Property<string>("txtClave")
                        .HasColumnType("NVARCHAR2(2000)")
                        .HasColumnName("TXT_CLAVE");

                    b.Property<string>("txtUsuario")
                        .HasColumnType("NVARCHAR2(2000)")
                        .HasColumnName("TXT_USUARIO");

                    b.HasKey("codUsuario");

                    b.ToTable("TC_USUARIO");
                });
#pragma warning restore 612, 618
        }
    }
}
