﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace convocatorias.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TC_USUARIO",
                columns: table => new
                {
                    COD_USUARIO = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    TXT_USUARIO = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    TXT_CLAVE = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    INT_ESTADO = table.Column<int>(type: "NUMBER(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TC_USUARIO", x => x.COD_USUARIO);
                });

            migrationBuilder.CreateTable(
                name: "TMM_CONVOCATORIA",
                columns: table => new
                {
                    COD_CONVOCATORIA = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    TXT_DESCRIPCION = table.Column<string>(type: "NVARCHAR2(250)", maxLength: 250, nullable: true),
                    TXT_AVISO_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    TXT_TDR_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    TXT_FORMATOS_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    FEC_FECHA_INICIO = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    FEC_FECHA_FIN = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    INT_ESTADO_TRAZABILIDAD = table.Column<int>(type: "NUMBER(10)", maxLength: 2, nullable: true),
                    FEC_FECHA_PUBLICACION = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    FEC_FECHA_VIGENTE = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    FEC_FECHA_CONCLUIDA = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    TXT_NRO_OS = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    TXT_OS_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    INT_ESTADO_REGISTRO = table.Column<int>(type: "NUMBER(10)", maxLength: 1, nullable: true),
                    FEC_FECHA_REGISTRO = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMM_CONVOCATORIA", x => x.COD_CONVOCATORIA);
                });

            migrationBuilder.CreateTable(
                name: "TMM_CONVOCATORIA_POSTULANTE",
                columns: table => new
                {
                    COD_CONVOCATORIA_POSTULANTE = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    INT_CONVOCATORIA = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    INT_POSTULANTE = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    INT_ESTADO_TRAZABILIDAD = table.Column<int>(type: "NUMBER(10)", maxLength: 2, nullable: false),
                    INT_ESTADO_REGISTRO = table.Column<int>(type: "NUMBER(10)", maxLength: 1, nullable: false),
                    FEC_FECHA_REGISTRO = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMM_CONVOCATORIA_POSTULANTE", x => x.COD_CONVOCATORIA_POSTULANTE);
                });

            migrationBuilder.CreateTable(
                name: "TMM_POSTULANTE",
                columns: table => new
                {
                    COD_POSTULANTE = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    INT_ANIO = table.Column<int>(type: "NUMBER(10)", maxLength: 4, nullable: true),
                    TXT_NRO_DOCUMENTO = table.Column<string>(type: "NVARCHAR2(8)", maxLength: 8, nullable: true),
                    TXT_NOMBRES = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    TXT_APELLIDOS = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    TXT_CORREO_ELECTRONICO = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    TXT_CELULAR = table.Column<string>(type: "NVARCHAR2(9)", maxLength: 9, nullable: true),
                    TXT_REGION_ID = table.Column<string>(type: "NVARCHAR2(2)", maxLength: 2, nullable: true),
                    TXT_PROVINCIA_ID = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    TXT_DISTRITO_ID = table.Column<string>(type: "NVARCHAR2(6)", maxLength: 6, nullable: true),
                    TXT_DIRECCION = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    TXT_FORMATOS_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    TXT_CV_COMERCIAL_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    TXT_CV_DOCUMENTADO_ARCHIVO = table.Column<string>(type: "NVARCHAR2(25)", maxLength: 25, nullable: true),
                    INT_ESTADO_REGISTRO = table.Column<int>(type: "NUMBER(10)", maxLength: 1, nullable: true),
                    FEC_FECHA_REGISTRO = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMM_POSTULANTE", x => x.COD_POSTULANTE);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TC_USUARIO");

            migrationBuilder.DropTable(
                name: "TMM_CONVOCATORIA");

            migrationBuilder.DropTable(
                name: "TMM_CONVOCATORIA_POSTULANTE");

            migrationBuilder.DropTable(
                name: "TMM_POSTULANTE");
        }
    }
}
