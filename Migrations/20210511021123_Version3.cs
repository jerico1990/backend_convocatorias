﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace convocatorias.Migrations
{
    public partial class Version3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TG_TRAZABILIDAD",
                columns: table => new
                {
                    COD_TRAZABILIDAD = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    TXT_DESCRIPCION = table.Column<string>(type: "NVARCHAR2(250)", maxLength: 250, nullable: true),
                    INT_ESTADO_REGISTRO = table.Column<int>(type: "NUMBER(10)", maxLength: 1, nullable: true),
                    FEC_FECHA_REGISTRO = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_TRAZABILIDAD", x => x.COD_TRAZABILIDAD);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TG_TRAZABILIDAD");
        }
    }
}
