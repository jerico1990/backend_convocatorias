﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace convocatorias.Migrations
{
    public partial class SecondCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FEC_FECHA_PUBLICACION",
                table: "TMM_CONVOCATORIA");

            migrationBuilder.AlterColumn<int>(
                name: "INT_ESTADO_TRAZABILIDAD",
                table: "TMM_CONVOCATORIA",
                type: "NUMBER(10)",
                maxLength: 2,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)",
                oldMaxLength: 2);

            migrationBuilder.AlterColumn<int>(
                name: "INT_ESTADO_REGISTRO",
                table: "TMM_CONVOCATORIA",
                type: "NUMBER(10)",
                maxLength: 1,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)",
                oldMaxLength: 1);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_VIGENTE",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_REGISTRO",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_INICIO",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_FIN",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_CONCLUIDA",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "INT_ESTADO_TRAZABILIDAD",
                table: "TMM_CONVOCATORIA",
                type: "NUMBER(10)",
                maxLength: 2,
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)",
                oldMaxLength: 2,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "INT_ESTADO_REGISTRO",
                table: "TMM_CONVOCATORIA",
                type: "NUMBER(10)",
                maxLength: 1,
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "NUMBER(10)",
                oldMaxLength: 1,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_VIGENTE",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_REGISTRO",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_INICIO",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_FIN",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FEC_FECHA_CONCLUIDA",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "TIMESTAMP(7)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FEC_FECHA_PUBLICACION",
                table: "TMM_CONVOCATORIA",
                type: "TIMESTAMP(7)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
